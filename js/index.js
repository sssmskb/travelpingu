const tab1 = document.querySelector('.tab1');
const tab2 = document.querySelector('.tab2');
const inclusions = document.querySelector('.inclusions');
const packageSection = document.querySelector('.package-section');

function changeTab(ele1, ele2, ele3, ele4) {    
    ele1.addEventListener('click', function() {
        ele1.classList.add('active');
        ele2.classList.add('hide');
        ele3.classList.remove('active');
        ele4.classList.remove('hide');
    }) 
}


changeTab(tab1, inclusions, tab2, packageSection);
changeTab(tab2, packageSection, tab1, inclusions);